**ItsyBitsyCloud**

ItsyBitsyCloud is a simple project to get a home cloud setup for file backups, private search, private bookmarking, and shared password management up and running quickly on a Raspberry Pi via containerization. Since we are using containers, the project can be adapted to other platforms with almost no changes.

The preferred distro for this project is [Void Linux](https://wiki.voidlinux.org/Raspberry_Pi). Since the project was originally based on Raspbian, documentation is still available, but Raspbian is no longer supported. Void was chosen because it provides proper 64-bit support for the Pi 3 and newer, and actually runs even lighter than Raspbian itself. The one caveat is that there is no Docker package available for 64-bit ARM, so we will be building it (instructions in the wiki, don't worry).

**Features:**
- [Bitwarden_RS](https://github.com/dani-garcia/bitwarden_rs) for shared password management.
- [SearX](https://searx.me/) for self-hosted search.
- [Syncthing](https://syncthing.net/) for file synchronization among multiple devices and backup.
- [Traefik](https://traefik.io/) is used as reverse proxy and to manage certificates.
- [Wallabag](https://wallabag.it) for bookmarking/read-it-later service.

**Requirements:**
- A Raspberry Pi
- A domain name
- A [Cloudflare](https://www.cloudflare.com/) account for DNS.

**Documentation:**
  - [Installation and Configuration (Void Linux)](https://gitlab.com/ferrettim/itsybitsycloud/-/wikis/Manual-Install-and-Configuration-(Void-Linux)) (Preferred)
  - [Installation and Configuration (Raspbian)](https://gitlab.com/ferrettim/itsybitsycloud/-/wikis/Manual-Install-and-Configuration-(Raspbian)) (Works, but unsupported)

**Feature Requests:**
  - For feature requests, please leave a comment in the [Feature Requests issue](https://gitlab.com/ferrettim/itsybitsycloud/issues/1) page instead of creating a new issue. Bugs should still be reported in the issue tracker.

**Supporting This Project:**
  - Code contributions and bug reports are welcome. If you'd like to support monetarily, donate to the respective projects listed in the **Features** section of this README, not me.
